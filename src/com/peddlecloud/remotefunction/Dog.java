package com.peddlecloud.remotefunction;

import java.util.Random;

public class Dog
{
    private String name;
    private static boolean bark = false;
    public enum Condition
    {
        IN, OUT
    }
    private Condition whereIsDog;// = Condition.IN;
    Dog(String name, Condition where)
    {
        this.name = name;
        this.whereIsDog = where;
    }
    Condition getWhereIsDog()
    {
        return whereIsDog;
    }
    void showWhere()
    {
        System.out.println(name + " is currently " + whereIsDog);
    }
    void bark()
    {
        Random random = new Random();
        bark = random.nextBoolean();
    }
    boolean isBarking()
    {
        return bark;
    }
    void toggleWhere()
    {
        if (whereIsDog == Dog.Condition.IN)
            whereIsDog = Dog.Condition.OUT;
        else
            whereIsDog = Dog.Condition.IN;
    }
    void toggleBark()
    {
        bark = !bark;
    }
}