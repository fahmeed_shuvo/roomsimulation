package com.peddlecloud.remotefunction;

import java.util.Scanner;

public class Remote extends RemoteControlHelper
{
    public static void main(String[] args)
    {
        int buttons;
        boolean flag = false;
        RemoteControlHelper helpRemote = new RemoteControlHelper();
        Devices[] dev = new Devices[4];
        dev[0] = new Devices("Fan", Devices.Status.OFF,0,0,0);
        dev[1] = new Devices("Light", Devices.Status.OFF,0,0,0);
        dev[2] = new Devices("AC", Devices.Status.OFF,0, 24,0);
        dev[3] = new Devices("Door", Devices.Status.OFF);
        Dog dog = new Dog("Dog", Dog.Condition.IN);
        Scanner input = new Scanner(System.in);

        helpRemote.introduction(dev,dog);
        buttons = input.nextInt();

        while (buttons != 0)
        {
            if (helpRemote.isDoor(dev[buttons-1]))
            {
                flag = helpRemote.openDoorForDog(dev[buttons-1],dog);
            }
            else
            {
                helpRemote.deviceControl(dev[buttons-1]);
            }

            flag = helpRemote.checkOnDog(dev,dog,flag);
            buttons = input.nextInt();
        }

        System.out.println("Remote app exiting. Goodbye.");
    }
}