package com.peddlecloud.remotefunction;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public class RemoteControlHelper
{
    void tryBark(@NotNull Dog dog)
    {
        if (!dog.isBarking())
        {
            dog.bark();
        }
    }
    boolean promptDog(Dog dog, Devices Door, boolean flag)
    {
        tryBark(dog);
        if (dog.isBarking())
        {
            if (Door.getStatus() == Devices.Status.OFF)
            {
                if (dog.getWhereIsDog() == Dog.Condition.IN)
                {
                    if (!flag)
                    {
                        System.out.println("\nYou Should open the door!");
                        flag = true;
                    }
                    else
                    {
                        System.out.println("\nIt is too late. He has done it.");
                        dog.toggleBark();
                        flag = false;
                    }
                }
                else
                {
                    System.out.println("\nYou Should let the dog in!");
                }
            }
        }
        return flag;
    }
    void doorForDog(@NotNull Dog dog, Devices Door)
    {
        if (dog.isBarking())
        {
            if (Door.getStatus() == Devices.Status.OFF)
            {
                if (dog.getWhereIsDog() == Dog.Condition.IN)
                {
                    System.out.println("You saved the floor.");
                }
                else
                {
                    System.out.println("You let the dog in.");
                }
                dog.toggleWhere();
            }
            dog.toggleBark();
        }
    }
    void welcomeMsg()
    {
        String[] Welcome = {"Welcome to remote control app. Enter Device number to press the corresponding button.",
                "Current Devices are: 1. Fan, 2. Light, 3. AC, 4. Door",
                "Pressing a button will change the device's current status.",
                //"Show button shows current status of every devices and the dog.)",
                "Enter 0 to exit the program."};
        for (String temp : Welcome) {
            System.out.println(temp);
        }
    }
    void showAll(@NotNull Devices[] dev, @NotNull Dog dog) //Shows status of everything
    {
        System.out.println("Current Status:");
        dog.showWhere();
        for (Devices devices : dev) {
            devices.showStatus(devices.getDeviceName());
        }
    }
    boolean isDeviceOn(@NotNull Devices device)
    {
        if (device.getStatus() == Devices.Status.OFF)
        {
            System.out.println("Turn on the " + device.getDeviceName() + " first.");
            return false;
        }
        else
        {
            return true;
        }
    }
    void changeParameter(char choice, Devices device, String type)
    {
        switch (choice)
        {
            case '+': case '-':
            if (isDeviceOn(device))
            {
                device.setParameter(choice,type);
                device.showStatus(device.getDeviceName());
            }
            break;
            default:
                System.out.println("Wrong choice. Prompt cancelled.");
                break;
        }
    }
    char controlParameterPrompt(String deviceParameter)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Press + to increase " + deviceParameter + ".");
        System.out.println("Press - to decrease " + deviceParameter + ".");
        return input.next().charAt(0);
    }
    void deviceControl(@NotNull Devices device)//choose a device to press corresponding buttons
    {
        char choice;
        Scanner input = new Scanner(System.in);

        System.out.println("What do you want to do?");
        System.out.println("Press 1 to turn " + device.getDeviceName() + " " + ((device.getStatus() == Devices.Status.ON) ? "off." : "on."));

        switch (device.getDeviceName())
        {
            case "AC":
                System.out.println("Press 2 to select speed.");
                System.out.println("Press 3 to select temperature.");
                choice = input.next().charAt(0);
                switch (choice)
                {
                    case '1':
                        device.setParameter(1, 24, 0);
                        device.showStatus(device.getDeviceName());
                        break;
                    case '2':
                        if (!isDeviceOn(device)) break;
                        choice = controlParameterPrompt("speed");
                        changeParameter(choice, device, "speed");
                        break;
                    case '3':
                        if (!isDeviceOn(device)) break;
                        choice = controlParameterPrompt("temperature");
                        changeParameter(choice, device, "temp");
                        break;
                    default:
                        System.out.println("Wrong choice. Prompt cancelled.");
                        break;
                }
                break;
            case "Fan":
                choice = controlParameterPrompt("speed");
                if (choice == '1') {
                    device.setParameter(1, 0, 0);
                    device.showStatus(device.getDeviceName());
                } else {
                    changeParameter(choice, device, "speed");
                }
                break;
            case "Light":
                choice = controlParameterPrompt("intensity");
                if (choice == '1') {
                    device.setParameter(0, 0, 1);
                    device.showStatus(device.getDeviceName());
                } else {
                    changeParameter(choice, device, "light");
                }
                break;
        }
    }
    void introduction(Devices[] dev, Dog dog)
    {
        welcomeMsg();
        showAll(dev,dog);
    }
    boolean checkOnDog(@NotNull Devices[] devices, Dog dog, boolean flag)
    {
        flag = promptDog(dog,devices[3],flag);
        showAll(devices,dog);
        return flag;
    }
    boolean openDoorForDog(Devices dev,Dog dog)
    {
        doorForDog(dog,dev);
        dev.setParameter(0,0,0);
        dev.showStatus(dev.getDeviceName());
        return false;
    }
    boolean isDoor(@NotNull Devices device)
    {
        return device.getDeviceName().equals("Door");
    }
}