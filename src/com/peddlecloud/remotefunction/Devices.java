package com.peddlecloud.remotefunction;

public class Devices extends DeviceParameterControl
{
    //private static Status status;
    private String deviceName;
    public enum Status
    {
        ON, OFF
    }
    private static Status status = Status.OFF;

    //constructors
    Devices(String name, Status status)
    {
        setDeviceName(name);
        setStatus(status);
    }
    Devices(String name, Status status, int speed, int temp, int light)
    {
        setDeviceName(name);
        setStatus(status);
        setSpeed(speed);
        setTemp(temp);
        setLight(light);
    }

    static void setStatus(Status newStatus) { status = newStatus; }
    static Status getStatus()
    {
        return status;
    }
    void showStatus(String deviceName)
    {
        if (status == Status.OFF)
        {
            System.out.println(deviceName + " is currently " + status);
        }
        else if (deviceName.equals("AC"))
        {
            System.out.println(deviceName + " is currently " + status + " with speed " + getSpeed() + " and with temperature " + getTemp());
        }
        else if (deviceName.equals("Light"))
        {
            System.out.println(deviceName + " is currently " + status + " with intensity of " + getLight());
        }
        else if (deviceName.equals("Fan"))
        {
            System.out.println(deviceName + " is currently " + status + " with speed " + getSpeed());
        }
        else
        {
            System.out.println(deviceName + " is currently " + status);
        }
    }
    void setDeviceName(String deviceName)
    {
        this.deviceName = deviceName;
    }
    String getDeviceName()
    {
        return deviceName;
    }
}