package com.peddlecloud.remotefunction;

import org.jetbrains.annotations.NotNull;

public class DeviceParameterControl
{
    //variables declaration
    private String[] parameterValue = {"null", "low", "medium", "high"};
    private int speed = 0;
    private int light = 0;
    private int temp = 0;

    int getValueOfParameter(@NotNull String parameter)
    {
        int value;
        switch (parameter)
        {
            case "speed":
                value = speed;
                break;
            case "temp":
                value = temp;
                break;
            case "light":
                value = light;
                break;
            default:
                value = 0;
                break;
        }
        return value;
    }
    void setValueOfParameter(@NotNull String parameter, int value)
    {
        switch (parameter)
        {
            case "speed":
                setSpeed(value);
                break;
            case "temp":
                setTemp(value);
                break;
            case "light":
                setLight(value);
                break;
        }
    }
    void setParameter(char sign, String parameter)
    {
        int valueOfParameter = getValueOfParameter(parameter);
        int maxValue, minValue;
        if (parameter.equals("temp"))
        {
            maxValue = 32;
            minValue = 16;
        }
        else
        {
            maxValue = 3;
            minValue = 1;
        }
        if (sign == '+')
        {
            if (valueOfParameter < maxValue)
            {
                valueOfParameter++;
            }
            else
            {
                System.out.println(parameter + " is already maximum.");
            }
        }
        else if (sign == '-')
        {
            if (valueOfParameter > minValue)
            {
                valueOfParameter--;
            }
            else
            {
                System.out.println(parameter + " is already minimum.");
            }
        }
        else
        {
            System.out.println("Wrong command.");
        }

        setValueOfParameter(parameter,valueOfParameter);
    }
    void setParameter(int speed, int temp, int light)
    {
        if (Devices.getStatus() == Devices.Status.ON)
        {
            Devices.setStatus(Devices.Status.OFF);
            setSpeed(0);
            setTemp(0);
            setLight(0);
        }
        else
        {
            Devices.setStatus(Devices.Status.ON);
            setSpeed(speed);
            setTemp(temp);
            setLight(light);
        }
    }

    //speed related methods
    void setSpeed(int speed)
    {
        this.speed = speed;
    }
    String getSpeed()
    {
        return parameterValue[speed];
    }

    //light related methods
    void setLight(int light)
    {
        this.light = light;
    }
    String getLight()
    {
        return parameterValue[light];
    }

    //temperature related methods
    void setTemp(int temp) {
        this.temp = temp;
    }
    int getTemp()
    {
        return temp;
    }
}